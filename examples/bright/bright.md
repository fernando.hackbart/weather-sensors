# BRIGHT

* https://tutorials-raspberrypi.com/mcp3008-read-out-analog-signals-on-the-raspberry-pi/
* https://tutorials-raspberrypi.com/photoresistor-brightness-light-sensor-with-raspberry-pi/

## Install requirements



## Run example

```bash
cd ~/projects/weather-sensors/bright
python3 bright.py
```

![Raspberry-Pi-Helligkeitssensor-Fototransistor-Steckplatine-600x477](Raspberry-Pi-Helligkeitssensor-Fototransistor-Steckplatine-600x477.png)