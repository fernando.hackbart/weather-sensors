# Installing MQTT on Raspberry pi


## Update operating system

```bash
sudo apt-get update
sudo apt-get upgrade
```

## Install mosquitto

```bash
sudo apt install -y mosquitto mosquitto-clients
sudo systemctl enable mosquitto.service
```

## Configure security

```bash
sudo systemctl stop mosquitto
sudo mosquitto_passwd -c /etc/mosquitto/passwd weathersensors

# Welcome1
```

## Subscribe messages
```bash
mosquitto_sub -h localhost -t weather-sensors
```

## Produce messages

```bash
mosquitto_pub -h localhost -t weather-sensors -m "Exemplo"
```

