# Docker configuration

## Install Docker on Raspberry Pi

```bash
curl -sSL https://get.docker.com | sh
```
